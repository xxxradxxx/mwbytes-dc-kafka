# mwbytes-dc-kafka

Contains docker-compose file for kafka with persistent storage

Use `docker-compose up` command in order to start application.
Kafka brokers will be available on your machine's ports 19092, 29092,39092.

Kafka(data folder) and Zookeeper(data and datalog) data stored within correspondent folders.

If you want to clean up already stored data, stop your docker compose environment with `docker-sompose rm` command, execute `cleanupData.sh` script, and start environment again.